import React, { useEffect, useState, useCallback } from 'react'
import { View, Text, StyleSheet } from 'react-native'
import { Icon } from 'react-native-elements'
import { useFocusEffect } from '@react-navigation/native'
import { firebaseApp } from '../../utils/config'
import firebase from "firebase/app"
import "firebase/firestore"
import ListPets from '../../components/Pets/ListPets'
const db = firebase.firestore(firebaseApp);

export default function DashboardScreen(props) {
    const { navigation } = props;
    const [pets, setpets] = useState([])
    const [user, setUser] = useState(null)
    const [totalpets, settotalpets] = useState(0)
    const [starPets, setstarPets] = useState(null)
    const [isLoading, setisLoading] = useState(false)
    const limitPets = 10;
    
    useEffect(() => {
        firebase.auth().onAuthStateChanged((userInfo)=>{
            setUser(userInfo);
        })
    }, [])

    useFocusEffect(
        useCallback(()=>{
            db.collection("pets").get().then((snap)=>{
                settotalpets(snap.size);
            })
            const resultPets = [];
            db.collection("pets")
            .orderBy("createAt", "desc")
            .limit(limitPets).get().then((response)=>{
                setstarPets(response.docs[response.docs.length-1]);
    
                response.forEach((doc)=>{
                    //console.log(doc.data())
                    const pet = doc.data();
                    pet.id = doc.id;
                    resultPets.push(pet);
    
                })
                setpets(resultPets);
            })
        }, [])
    )



    const handleLoadMore = () =>{
        const resultPets = [];
        pets.length < totalpets && setisLoading(true);
        db.collection("pets")
            .orderBy("createAt", "desc")
            .startAfter(starPets.data().createAt)
            .limit(limitPets)
            .get()
            .then(response=>{
                if (response.docs.length > 0) {
                    setstarPets(response.docs[response.docs.length -1])
                }else{
                    setisLoading(false);
                }
                response.forEach((doc) =>{
                    const pet = doc.data();
                    pet.id = doc.id;
                    resultPets.push(pet);
                });
                setpets([...pets, ...resultPets])
            })
    }

    return (
        <View style={styles.viewBody}>
            <ListPets
                pets={pets} 
                handleLoadMore={handleLoadMore}
                isLoading={isLoading}
            />
            { user &&  (<Icon
                type="material-community"
                name="plus"
                color="#E74C3C"
                reverse
                containerStyle={styles.btnContainer} 
                onPress={()=> navigation.navigate("AddPets")}
            />)}
        </View>
    )
}

const styles = StyleSheet.create({
    viewBody: {
        flex: 1,
        backgroundColor: "#fff"
    },
    btnContainer: {
        position: "absolute",
        bottom: 10,
        right: 10,
        shadowColor: "black",
        shadowOffset:{ width: 2, height: 2},
        shadowOpacity: 0.5
    }
})
