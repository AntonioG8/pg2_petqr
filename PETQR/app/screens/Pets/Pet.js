import React, {useEffect, useState} from 'react'
import { View, Text, ActivityIndicator, StyleSheet, ScrollView, Dimensions } from 'react-native'
import { Image, ListItem, Icon } from 'react-native-elements';
import { firebaseApp } from '../../utils/config'
import { map } from 'lodash'
import Loading from '../../components/Loading'
import CarouselImages from '../../components/CarouselImages'
import Map from '../../components/Map'
import firebase from 'firebase/app'
import "firebase/firestore"

const db = firebase.firestore(firebaseApp)
const screenWidth = Dimensions.get("window").width;

export default function Pet(props) {
    const {navigation, route } = props
    const { id, name} = route.params
    const [pet, setpet] = useState(null)
    console.log(route)

    navigation.setOptions({title: name})

    useEffect(() => {
        db.collection("pets")
            .doc(id)
            .get()
            .then((response)=>{
                const data = response.data();
                data.id = response.id;
                setpet(data);
            })
    }, []);

    if (!pet) return <Loading isVisible = {true} text="Cargando.."/>;
    

    return (
        <ScrollView vertical style={styles.viewBody}>
            <CarouselImages
                arrayImages = {pet.images}
                height = {250}
                width={screenWidth}
            />
            <TitlePet 
                name={pet.name}
                description = {pet.description}
                age = {pet.age}
            />
            <PetInfo 
                location = {pet.location}
                name = {pet.name}
                address = {pet.address}
            />
            
        </ScrollView>
    )
}

function TitlePet(props) {
    const { name, description, age } = props
    return(
        <View style={styles.viewPetTitle}>
            <View style = {{flexDirection: "row"}}>
                <Text style={styles.namePet}>El nombre de la mascota es: {name}</Text>
            </View>
            <Text style={styles.descriptionPet}>Descripcion de la mascota</Text>
            <Text >{description}</Text>
            <Text style={styles.agePet}>Edad de la masctoa</Text>
            <Text >{age} años</Text>
        </View>
    )
}

function PetInfo(props){
    const { location, name, address } = props

    const listInfo = [{
        text: address,
        iconName: "map-marker",
        iconType: "material-community",
        action: null
    }]
    return(
        <View style = {styles.viewPetInfo}>
            <Text style={styles.petInfoTitle}>
                Informacion sobre la mascota {name}
            </Text>
            <Map
                location={location}
                name = {name}
                height = {200}
            />
            {map(listInfo, (item, index)=> (
                <ListItem 
                    key={index}

                    leftIcon={{
                        name: item.iconName,
                        type: item.iconType,
                        color: "#EE4548"
                    }}
                    containerStyle={styles.containerListItem}
                >
                    <ListItem.Title>
                        {item.text}
                    </ListItem.Title>
                </ListItem>
            ))}
        </View>
    )
}

const styles = StyleSheet.create({
    viewBody: {
        flex: 1,
        backgroundColor: "#fff"
    },
    viewPetTitle:{
        padding: 15
    },
    namePet: {
        fontSize: 20,
        fontWeight: "bold"
    },
    descriptionPet: {
        marginTop: 5,
        color: "#EE4548"
    },
    agePet: {
        marginTop: 5,
        color: "#EE4548"
    },
    viewPetInfo: {
        margin: 15,
        marginTop: 25
    },
    petInfoTitle: {
        fontSize: 20,
        fontWeight: "bold",
        marginBottom: 20
    },
    containerListItem: {
        borderBottomColor: "#d8d8d8",
        borderBottomWidth: 1,
    }
})