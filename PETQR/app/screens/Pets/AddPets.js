import React, {useState, useRef} from 'react'
import { View } from 'react-native'
import Toast from 'react-native-easy-toast'
import Loading from '../../components/Loading'
import AddPetsForm from '../../components/Pets/AddPetsForm'
export default function AddPets(props) {
    const { navigation }=props;
    const [isLoading, setisLoading] = useState(false)
    const toastRef = useRef();
    
    return (
        <View>
            <AddPetsForm 
                toastRef={toastRef}
                setisLoading={setisLoading}
                navigation={navigation}
            />
            <Toast ref={toastRef}  position="center" opacity={0,9}/>
            <Loading isVisible={isLoading} text="Creando mascotas"/>
        </View>
    )
}

