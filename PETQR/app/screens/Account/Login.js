import { useNavigation } from '@react-navigation/native'
import React, {useRef} from 'react'
import { View, Text, ScrollView, Image, StyleSheet } from 'react-native'
import { Divider } from 'react-native-elements'
import LoginForm from '../../components/Account/LoginForm'
import Toast from 'react-native-easy-toast'
export default function Login() {
    const toastRef = useRef();
    return (
        <ScrollView>    
            <Image 
                source = {require("../../../assets/ubicacion.png")}
                resizeMode="contain"
                style={styles.logo}
            />
            <View style={styles.viewContainer}>
                <LoginForm toastRef={toastRef}/> 
                <CreateAccount/>
            </View>
            
            <Divider style={styles.divider}/>
            <Text style={styles.txtSocial}>Social Login</Text>
            <Toast ref={toastRef} position="center" opacity={0,9}/>
        </ScrollView>
    );
}
//Cuando se vaya a crear un componente este siempre debe inicar con una letra mayuscula
function CreateAccount(){
    
    const navigation = useNavigation();
    return(
        <Text style={styles.textRegister}>
            Aun no tienes una cuenta?{" "}
            <Text 
                style={styles.btnRegister}
                onPress={()=>navigation.navigate("Registro")}>
                Registrate
            </Text>
        </Text>
    );
}

const styles = StyleSheet.create({ 
    logo:{
        width: "100%",
        height: 150,
        marginTop: 20
    },
    viewContainer:{
        marginRight: 40,
        marginLeft: 40
    },
    textRegister: {
        marginTop: 15,
        marginLeft: 10,
        marginRight: 10
    },
    btnRegister: {
        color: "#40B02E",
        fontWeight: "bold"
    },
    divider: {
        backgroundColor: "#40B02E",
        margin: 40,
    },
    txtSocial: {
        marginTop: 10,
        marginLeft: 20,
        marginRight: 20
    }
})