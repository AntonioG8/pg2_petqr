import React, { Component } from 'react'
import { useNavigation } from '@react-navigation/native'
import { StyleSheet, View, ScrollView, Text } from 'react-native'
import { Button } from 'react-native-elements'


export default function UserGuest() {
    const navigation = useNavigation();
    return (
       <ScrollView centerContent={true} style={styles.viewBody}>
           <Text style={styles.title}>
                Bienvenido a tu perfil de PETQR 
           </Text>
           <Text style={styles.description}>
               Consulta la informacion de tu mascota y busca informacion de mascotas
               si estas se encuentran extraviadas, apoyando a devolverlas a su hogar.
           </Text>
           <View style={styles.viewBtn}>
                <Button
                    title="Ver tu perfil, o inicia sesion!"
                    buttonStyle={styles.btnStyle}
                    containerStyle={styles.btnContainer}
                    onPress={()=> navigation.navigate("Login")}
                />
           </View>
       </ScrollView>
    )
}

const styles = StyleSheet.create({
    viewBody: {
        marginLeft: 30,
        marginRight: 30,
    },
    title:{
        padding: 50,
        fontWeight: "bold",
        fontSize: 30,
        marginBottom: 10,
        textAlign: "center"
    },
    description: {
        padding: 30,
        textAlign: "center",
        marginBottom: 50,
        fontSize: 20
    },
    viewBtn: {
        flex:1,
        alignItems: "center",
        justifyContent: "center"
    },
    btnStyle: {
        backgroundColor: "#B03A2E"
    },
    btnContainer: {
        width:"70%" ,
        padding: 50  
    },
  
})