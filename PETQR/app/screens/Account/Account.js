import React, { useState, useEffect } from 'react'
import * as Google from 'expo-google-app-auth';
import {androidClientId} from '../../utils/config';
import * as firebase from "firebase"

import UserGuest from "./UserGuest";
import UserLogged from "./UsuerLogged";
import Loading from "../../components/Loading"

export default function Account() {

  const [login, setLogin]=useState(null);
  
  useEffect(() => {
    firebase.auth().onAuthStateChanged((user) =>{
      console.log(user);
      !user ? setLogin(false) : setLogin(true)//operador ternario para verificar el estado del usuario
    })
  }, []);

  if(login === null) return <Loading isVisible={true} text="Cargando usuario..."/>; 
  
  return login ? <UserLogged/> : <UserGuest/>
}


