import React, { useState, useEffect } from 'react'
import { View, StyleSheet, Text, FlatList, Image } from 'react-native'
import { SearchBar, ListItem, Icon, Avatar } from 'react-native-elements'
import { FireSQL } from 'firesql'
import firebase from "firebase/app"

const fireSQL = new FireSQL(firebase.firestore(), {includeId: "id"});                
export default function Search(props) {
   
    const { navigation } = props;
    const [search, setsearch] = useState("");
    const [pets, setpets] = useState([])


    useEffect(() => {
        if (search) {
            fireSQL.query(`SELECT * FROM pets WHERE name LIKE '${search}%'`)
            .then((response)=>{
                console.log(response)
                setpets(response)
            })
        }
    }, [search])

    return( 
        <View>
            <SearchBar
                placeholder = "Busca a tu mascota"
                onChangeText={(e)=>setsearch(e)}
                value={search}
                containerStyle={styles.searchBar}
            />
            {pets.length === 0 ? (
                <NoFoundPets/>
            ):(
                <FlatList
                    data={pets}
                    renderItem={(pet)=> 
                        <Pet pet={pet} navigation={navigation}/>
                    }
                    keyExtractor={(item, index) => index.toString()}
                />
            )}
        </View>
    )
}

function NoFoundPets(){
    return(
        <View style={{flex: 1, alignItems: "center"}}>
            <Image
                source={require("../../assets/no-result-found.png")}
                resizeMode="cover"
                style={{width: 200, height: 200}}
            />
        </View>
    )
}

function Pet(props){
    const { pet, navigation } = props
    const { id, name, images } = pet.item
    return ( 
        <ListItem
            /*title={name}
            leftAvatar={{
                source: images[0] ? {uri: images[0]} : require("../../assets/no-image.png")
            }}*/      
            onPress={() => navigation.navigate("Pets", {
                screen: "Pet",
                params: {id}
            })} 
        >

            <Avatar rounded source={images[0] ? {uri: images[0]} : require("../../assets/no-image.png")}/>
            <ListItem.Title>{name}</ListItem.Title>
            <ListItem.Chevron color="#121212" />
        </ListItem>
    )
}

const styles = StyleSheet.create({
    searchBar: {
        marginBottom: 20
    }
})