import firebase from "firebase/app"

const firebaseConfig = {
  apiKey: "AIzaSyDBPK4-BPn1Watgoi6TdScViR0NKHMcD2A",
  authDomain: "petqr-backend.firebaseapp.com",
  databaseURL: "https://petqr-backend.firebaseio.com",
  projectId: "petqr-backend",
  storageBucket: "petqr-backend.appspot.com",
  messagingSenderId: "937669936518",
  appId: "1:937669936518:web:7a1f8f3757a88c77b39807"
};

export const firebaseApp = firebase.initializeApp(firebaseConfig)

export const androidClientId = '937669936518-tqp55fn4em6qajdt2j6d2995sdp2pblb.apps.googleusercontent.com';