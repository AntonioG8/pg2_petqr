import React from 'react'
import { createStackNavigator } from '@react-navigation/stack'
import Account from "../screens/Account/Account"
import Login from "../screens/Account/Login"
import Register from "../screens/Account/Register"
const Stack = createStackNavigator();

export default function AccountStack() {
    return (
        <Stack.Navigator>
            <Stack.Screen
                name="Cuenta"
                component = {Account}
                options = {{title:"Cuenta de PetQR"}}
            />
            <Stack.Screen
                name="Login"
                component={Login}
                options = {{title:"Login PETQR"}}
            />
            <Stack.Screen
                name="Registro"
                component={Register}
                options = {{title: "Registro de Usuarios"}}
            />
        </Stack.Navigator>
    )
}
