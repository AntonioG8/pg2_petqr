import React from 'react'
import { NavigationContainer } from "@react-navigation/native"
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs"
import { Icon } from "react-native-elements"

import SearchStack from "../navigations/SearchStack";
import PetsStack from "../navigations/PetsStack";
import AccountStack from "./AccountStack";
import CameraStack from "./CameraStack";


const Tab = createBottomTabNavigator();


export default function Navigation() {
    return (
        <NavigationContainer>
            <Tab.Navigator 
                initialRouteName="Login"
                tabBarOptions={{
                    activeTintColor: 'tomato',
                    inactiveTintColor: 'gray',
                }}
                screenOptions={({ route }) =>({
                    tabBarIcon: ({ color }) => screenOptions(route,color)
                })}
                >
                <Tab.Screen
                    name="Busqueda"
                    component={SearchStack}
                    options={{title:"Busqueda"}}
                />
                <Tab.Screen
                    name="Pets"
                    component={PetsStack}
                    options={{title:"Mascotas"}}
                />
                <Tab.Screen
                    name="Login"
                    component={AccountStack}
                    options={{title:"Cuenta"}}
                />
                <Tab.Screen
                    name="Scanner"
                    component={CameraStack}
                    options={{title:"Scanner"}}
                />

            </Tab.Navigator>
        </NavigationContainer>
    )
}

function screenOptions(route, color){
    let iconName;

    switch(route.name){
        case "Pets":
            iconName = "paw"
            break;
        case "Login":
            iconName = "account-circle-outline"
            break;
        case "Busqueda":
            iconName = "account-search"
            break;
        case "Scanner":
            iconName = "barcode-scan"
            break;    
        default:
            break;
    }
    return (
        <Icon type="material-community" name={iconName} size={22} color={color}/>
    )
}