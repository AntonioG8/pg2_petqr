import React, { Component } from 'react'
import { createStackNavigator } from '@react-navigation/stack'
import DashboardScreen from "../screens/Pets/DashboardScreen"
import AddPets from '../screens/Pets/AddPets'
import Pet from '../screens/Pets/Pet'
const Stack = createStackNavigator();

export default function PetsStack() {
    return (
        <Stack.Navigator>
            <Stack.Screen
                name="Pantalla_principal"
                component = {DashboardScreen}
                options = {{title:"Pantalla principal"}}
            />
            <Stack.Screen
                name="AddPets"
                component = {AddPets}
                options = {{title:"Agregar Mascotas"}}
            />
            <Stack.Screen
                name="Pet"
                component = {Pet}
                options={{title: "Informacion de la mascota"}}
            />
        </Stack.Navigator>
    )
}
