import React, { Component } from 'react'
import { createStackNavigator } from '@react-navigation/stack'
import CameraScreen from "../screens/CameraScreen"

const Stack = createStackNavigator();

export default function CameraStack() {
    return (
        <Stack.Navigator>
            <Stack.Screen
                name="Camera"
                component = {CameraScreen}
                options = {{title:"Scanner de PETSQRS"}}
            />
        </Stack.Navigator>
    )
}
