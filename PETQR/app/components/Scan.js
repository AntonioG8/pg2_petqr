import React, { useState, useEffect }  from 'react'
import { View, Text, StyleSheet, Button } from 'react-native'
import { BarCodeScanner } from 'expo-barcode-scanner';

export default function Scan() {
    const [hasPermission, setHasPermission] = useState(null);
    const [scanned, setScanned] = useState(false);
    const [modalVisible, setModalVisible] = useState(true);
    useEffect(() => {
        (async () => {
            const { status } = await BarCodeScanner.requestPermissionsAsync();
            setHasPermission(status === 'granted');
        })();
    }, []);
   /* const pet = {
        nombre: "canela",
        raza: "labrador"
    }*/
    const handleBarCodeScanned = ({ data }) => {
        setScanned(true);
        alert(`El nombre de la mascota es ${data}`);
    };

    if (hasPermission === null) {
        return <Text>Esperando para obtener acceso a la camara</Text>;
    }
    if (hasPermission === false) {
        return <Text>No se puede acceder a la camara</Text>;
    }

    return (
        <View
        style={{
            flex: 1,
            flexDirection: 'column',
            justifyContent: 'flex-end',
        }}>
        <BarCodeScanner
            onBarCodeScanned={scanned ? undefined : handleBarCodeScanned}
            style={StyleSheet.absoluteFillObject}
        />

        {scanned && <Button title={'Toca para escanear de nuevo'} onPress={() => setScanned(false)} />}
        </View>
    );
}
