import React from 'react'
import { View, Text, StyleSheet, FlatList, ActivityIndicator, TouchableOpacity } from 'react-native'
import { Image } from 'react-native-elements'
import { size } from "lodash"
import {  useNavigation } from "@react-navigation/native"


export default function ListPets(props) {
    const { pets, handleLoadMore, isLoading } = props;
    const navigation = useNavigation();
    return (
        <View>
            {size(pets) > 0 ? (
                <FlatList
                    data={pets}
                    renderItem={(pet)=><Pet pet={pet} navigation={navigation}/>}
                    keyExtractor={(item, index)=> index.toString()}
                    onEndReachedThreshold={0.5}
                    onEndReached={handleLoadMore}
                    ListFooterComponent={<FooterList isLoading={isLoading}/>}
                />
            ):(
                <View style={styles.loaderPets}>
                    <ActivityIndicator size="large"/>
                    <Text>Cargando mascotas</Text>
                </View>) 
            }
        </View>
    )
}

function Pet(props){
    const {pet, navigation} = props;
    const { id, images, name, description, address } = pet.item
    const imagePet = images ? images[0] : null;

    const goPet = () =>{
        navigation.navigate("Pet", {
            id,
            name,
        })
    }
    return(
        <TouchableOpacity 
            onPress={goPet}
        >
            <View style={styles.viewPet}>
                <View style={styles.viewPetImage}>
                    <Image
                        resizeMode="cover"
                        PlaceholderContent={<ActivityIndicator color="#45EEB4"/>}
                        source={
                            imagePet
                            ? {uri: imagePet}
                            : require('../../../assets/no-image.png')
                        }
                        style={styles.imagePet}
                    />
                </View>
                <View>
                    <Text style={styles.petName}>{name}</Text>
                    <Text style={styles.petAddress}>{address}</Text>
                    <Text style={styles.petDescription}>{description.substr(0, 60)}...</Text>
                </View>
            </View>
        </TouchableOpacity>
    )
}

function FooterList(props) { 
    const { isLoading } = props;
    if (isLoading) {
        return(
            <View style={styles.loaderPets}>
                <ActivityIndicator size="large" />
            </View>
        )
    }else{
        return ( 
            <View style={styles.notFoundPets}>
                <Text>No quedan mascotas por cargar</Text>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    loaderPets:{
        marginTop: 10,
        marginBottom: 10,
        alignItems: "center"
    },
    viewPet: {
        flexDirection: "row",
        margin: 10
    },
    viewPetImage: {
        marginRight: 15,
    },
    imagePet: {
        width: 80,
        height: 80
    },
    petName: {
        fontWeight: "bold",
    }, 
    petAddress: {
        paddingTop: 2,
        color: "grey"
    },
    petDescription: {
        paddingTop: 2,
        color: "grey",
        width: 300,
    }, 
    notFoundPets: {
        marginTop: 10,
        marginBottom: 20,
        alignItems: "center"
    }
})