import React, {useState, useEffect} from 'react'
import { View, StyleSheet, ScrollView, Alert, Dimensions } from 'react-native'
import { Input, Icon, Avatar, Image, Button } from 'react-native-elements'
import * as Permissions from 'expo-permissions'
import * as ImagePicker from 'expo-image-picker'
import { map, size, filter, result } from "lodash"
import * as Location from 'expo-location'
import Modal from '../Modal'
import MapView from 'react-native-maps';
import {firebaseApp} from "../../utils/config"
import firebase from "firebase/app"
import uuid from "random-uuid-v4"
import "firebase/storage"
import "firebase/firestore"

const db = firebase.firestore(firebaseApp);

const WidthScreen = Dimensions.get("window").width;

export default function AddPetsForm(props) {
    const { toastRef, setisLoading, navigation } = props;

    const [petname, setpetname] = useState("")
    const [petaddress, setpetaddress] = useState("")
    const [agepet, setagepet] = useState(0)
    const [typepet,settypepet] = useState("")
    const [petDescription, setpetDescription] = useState("")
    const [imageSelected, setimageSelected] = useState([])
    const [isVisibleMap, setisVisibleMap] = useState(false)
    const [locationPet, setlocationPet] = useState(null)

    const addPet = () => {
        if (!petname || !petaddress || !agepet || !typepet || !petDescription) {
            toastRef.current.show("Todos los campos deben estar llenos..");
        }else if (size(imageSelected) === 0) {
            toastRef.current.show("Debe subirse al menos una foto de la mascota");
        }else if(!locationPet){
            toastRef.current.show("Debe agregar una localizacion para la mascota..")
        }else{
            setisLoading(true)
            uploadImageStorage().then((response)=>{
               
                db.collection("pets")
                    .add({
                        name : petname,
                        address : petaddress,
                        age : agepet,
                        type : typepet,
                        description : petDescription,
                        location : locationPet,
                        images : response,
                        createAt : new Date(),
                        createBy : firebase.auth().currentUser.uid,
                    })
                    .then(()=>{
                        setisLoading(false);
                        navigation.navigate("Pantalla_principal");
                    }).catch(()=>{
                        setisLoading(false);
                        alert("Error al agregar la mascota, intentelo más tarde..")
                    })
            });
            
        }
    }
    
    const uploadImageStorage = async () =>{
        const imageBlob = [];
        await Promise.all(
            map(imageSelected, async (image)=>{
                const response = await fetch(image);
                const blob = await response.blob();
                const ref = firebase.storage().ref("images_pets").child(uuid());
                await ref.put(blob).then(async(result)=>{
                    await firebase
                        .storage()
                        .ref(`images_pets/${result.metadata.name}`)
                        .getDownloadURL()
                        .then(photoUrl =>{
                            imageBlob.push(photoUrl);
                        })
                })
            })
        )
        return imageBlob;
    }; 

    return (
        
        <ScrollView style={styles.scrollView}>
            <ImagePet imagenPet={imageSelected[0]}/>
            <FormAdd
                setpetname={setpetname}
                setpetaddress={setpetaddress}
                setagepet={setagepet}
                settypepet={settypepet}
                setpetDescription={setpetDescription}
                setisVisibleMap={setisVisibleMap}
                locationPet={locationPet} 
            />
            <UploadImage 
                toastRef={toastRef} 
                setimageSelected={setimageSelected}
                imageSelected={imageSelected}
                   
            />
            <Button
                title="Añadir Mascota"
                onPress={addPet}
                buttonStyle={styles.btnaddPet}
            />
            <Map 
                isVisibleMap={isVisibleMap} 
                setisVisibleMap={setisVisibleMap} 
                setlocationPet={setlocationPet} 
                toastRef={toastRef}
            />
        </ScrollView>
    )
}

//Creando componente interno
function FormAdd(props){
    const{ 
        setpetname, 
        setpetaddress, 
        setagepet, 
        settypepet, 
        setpetDescription,
        setisVisibleMap,
        locationPet
    } = props;

    return(
        <View style={styles.viewForm}>
            <Input
                placeholder="Nombre de la mascota"
                containerStyle={styles.input}
                onChange={(e)=> setpetname(e.nativeEvent.text)}
            />
            <Input
                placeholder="Direccion de la mascota"
                containerStyle={styles.input}
                onChange={(e)=> setpetaddress(e.nativeEvent.text)}
                rightIcon = {{
                    type: "material-community",
                    name: "google-maps",
                    color: locationPet ? "#45EEB4" : "#c2c2c2",
                    onPress: () => setisVisibleMap(true)
                }}

            />
            <Input
                placeholder="Edad de las mascota"
                containerStyle={styles.input}
                onChange={(e)=> setagepet(e.nativeEvent.text)}
            />
             <Input
                placeholder="Tipo de mascota. Ejemplo (perro)"
                containerStyle={styles.input}
                onChange={(e)=> settypepet(e.nativeEvent.text)}
            />
            <Input
                placeholder="Descripcion de la mascota"
                multiline={true}
                inputContainerStyle={styles.textArea}
                onChange={(e)=> setpetDescription(e.nativeEvent.text)}
            />
        </View>
    )
}

function ImagePet(props){ 
    const { imagenPet } = props;
    return(
        <View style={styles.viewPhoto}>
            <Image 
                source={
                    imagenPet 
                    ?{uri: imagenPet}
                    : require("../../../assets/no-image.png")
                }
                style={{width: WidthScreen, height: 200}}
            />
        </View>
    )
}

 function Map(props){
    const { isVisibleMap, setisVisibleMap, setlocationPet, toastRef} = props;
    const [location, setlocation] = useState(null)
    useEffect(() => {
        (async()=>{
            const resultPermissions = await Permissions.askAsync(
                Permissions.LOCATION
            )
            const statusPermissions = resultPermissions.permissions.location.status;

            if(statusPermissions === "denied"){
                toastRef.current.show(
                    "Tienes que aceptar los permisos de aplicacion para crear una mascota",3000)
            }else{
                const loc = await Location.getCurrentPositionAsync({});
                const { status} = await Location.requestPermissionsAsync();
                //console.log(status);
                setlocation({
                    latitude: loc.coords.latitude,
                    longitude: loc.coords.longitude,
                    latitudeDelta: 0.001,
                    longitudeDelta: 0.001
                })
            }
        })()
    }, [])

    const confirmLocation = () =>{
        setlocationPet(location);
        toastRef.current.show("Localizacion guardada correctamente");
        setisVisibleMap(false);
    }
    return(
        <Modal isVisible = {isVisibleMap} setisVisible={setisVisibleMap}>
            <View>
                {location && (
                    <MapView
                        style={styles.mapStyle}
                        initialRegion={location}
                        showsUserLocation={true}
                        onRegionChange={(region)=> setlocation(region) }
                    >
                        <MapView.Marker
                            coordinate={{
                                latitude: location.latitude,
                                longitude: location.longitude
                            }}
                            draggable
                            image={require("../../../assets/pet-house.png")}   
                        >
                            
                        </MapView.Marker>
                    </MapView>    
                )}
                <View style={styles.viewMapBtn}>
                    <Button 
                        title = "Guardar ubicacion"
                        containerStyle={styles.viewMapBtnContainerSave}
                        buttonStyle={styles.viewMapBtnSave}
                        onPress={confirmLocation}
                    />
                    <Button 
                        title = "Cancelar ubicacion" 
                        containerStyle={styles.viewMapBtnContainerCancel}
                        buttonStyle={styles.viewMapBtnCancel}
                        onPress={()=> setisVisibleMap(false)}
                    />
                </View>
            </View>
        </Modal>
    )
}

function UploadImage(props){
    const { toastRef,imageSelected, setimageSelected } = props;

    const imageSelect = async ()=>{
        const resultPermissions = await Permissions.askAsync(
            Permissions.CAMERA_ROLL
        );
        if (resultPermissions === "denied") {
            toastRef.current.show("Es necesario aceptar los permisos de la galeria, si los has rechazado debes activarlos manualmente",3000);
        } else {
            const result = await ImagePicker.launchImageLibraryAsync({
                allowsEditing: true,
                aspect: [4, 3]
            });
            if (result.cancelled) {
                toastRef.current.show("Has cerrado la galeria sin seleccionar ninguna imagen", 2000);
            }else{
                setimageSelected([...imageSelected, result.uri]);
            }
        }
    };
    
    const removeImage = (image) =>{
        
        Alert.alert(
            "Eliminar imagen",
            "Estas seguro de eliminar la imagen?",
            [
                {
                    text: "Cancel",
                    style: "cancel"
                },
                {
                    text: "Eliminar",
                    onPress: ()=>{
                        setimageSelected(filter(
                            imageSelected, 
                            (imageUrl) => imageUrl !== image)
                        )
                    }
                }
            ],
            {cancelable: false}
        )
    }

    return(
        <View style = {styles.viewImages}>
            {size(imageSelected) < 5 && (
                <Icon
                    type="material-community"
                    name="camera"
                    containerStyle={styles.containerIcon}
                    onPress={imageSelect}
                />
            )}   
            {map(imageSelected, (imagepet, index)=>(
                <Avatar
                    key={index}
                    style={styles.miniaturaStyle}
                    source ={{uri: imagepet }}
                    onPress={()=>removeImage(imagepet)}
                />
            ))}
        </View>
    )
}

const styles = StyleSheet.create({
    scrollView:{
        height: "100%",
    },
    viewForm: {
        marginLeft: 10,
        marginRight: 10,
    },
    input: {
        marginBottom: 10,
    },
    textArea: {
        height: 100,
        width: "100%",
        padding: 0,
        margin: 0
    },
    btnaddPet: {
        backgroundColor: "#EE4560",
        margin: 20,
        borderRadius: 10,   
    },
    viewImages: {
        flexDirection: "row",
        marginLeft: 20,
        marginRight: 20,
        marginTop: 30,
    },
    containerIcon: {
        alignItems: "center",
        justifyContent: "center",
        marginRight: 10,
        height: 70,
        width: 70,
        backgroundColor: "#e3e3e3"
     },
     miniaturaStyle: {
         width: 70,
         height: 70,
         marginRight: 10
     },
     viewPhoto: {
         alignItems: "center",
         height: 200,
         marginBottom: 20,
     },
     mapStyle: {
         width: "100%",
         height: 550,
     },
     viewMapBtn: {
         flexDirection: "row",
         justifyContent: "center",
         marginTop: 10
     },
     viewMapBtnContainerCancel: {
         paddingLeft: 5
     },
     viewMapBtnCancel: {
         backgroundColor: "#EE4560",
         borderRadius: 5
     },
     mapViewMarkerImg: {
         width: 100,
         height: 100
     },
     viewMapBtnContainerSave: {
         paddingRight: 5
     },
     viewMapBtnSave: {
        backgroundColor: "#45EEB4",
        borderRadius: 5
     }
})