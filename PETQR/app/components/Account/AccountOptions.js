import React from 'react'
import { View, Text, StyleSheet } from 'react-native'
import { ListItem, Avatar, Icon } from 'react-native-elements'
import {map} from "lodash"
export default function AccountOptions(props) {
    const {userInfo, toastRef} = props;
   
    const selectComponent = (key) =>{
        console.log("click")
        console.log(key)
    }
    const menuOptions = generateOptions(selectComponent);

    return (
        <View>
            {
                menuOptions.map((item, i)=>(
                    <ListItem key={i} bottomDivider containerStyle={styles.menuItem} onPress={item.onPress}>
                        <Icon name={item.iconNameLeft}/>
                        <ListItem.Content>
                            <ListItem.Title>{item.title}</ListItem.Title>
                        </ListItem.Content>
                        <ListItem.Chevron/>
                    </ListItem>
                ))
            }
        </View>
    )
}
function generateOptions(selectComponent){
    return[
    {
        title: "Cambiar Nombre y Apellidos",
        iconType: "material-community",
        iconNameLeft: "account-circle",
        iconNameRight: "chevron-right",
        onPress: ()=> selectComponent("displayName")
    },
    {
        title:"Cambiar Email",
        iconType: "material-community",
        iconNameLeft: "email",
        iconNameRight: "chevron-right",
        onPress: ()=> selectComponent("email")
    },
    {
        title: "Cambiar contraseña",
        iconType: "material-community",
        iconNameLeft: "lock",
        iconNameRight: "chevron-right",
        onPress: ()=> selectComponent("password")
    },
];
}
const styles = StyleSheet.create({
    menuItem:{
        borderBottomWidth: 1,
        borderBottomColor: "#e3e3e3"
    }
})