import React from 'react';
import { View, Text, StyleSheet } from 'react-native';
import { Avatar, Accessory } from 'react-native-elements';
import * as firebase from 'firebase';
import * as Permissions from 'expo-permissions';
import * as ImagePicker from 'expo-image-picker';
export default function InfoUser(props) {
    
    const{ 
        userInfo : {photoURL, displayName, email, uid},
        toastRef,
        setloading,
        setloadingText,
    } = props;
    
    const changeAvatar = async () =>{
        const resultPermission = await Permissions.askAsync(Permissions.CAMERA_ROLL);
        const resultPermissionCamera = resultPermission.permissions.cameraRoll.status;
        if (resultPermissionCamera === "denied") {
            toastRef.current.show("Es necesario brindar permisos a la aplicacion")
        }else{
            const result = await ImagePicker.launchImageLibraryAsync({
                allowsEditing: true,
                aspect: [4,3]
            })
            if (result.cancelled) {
                toastRef.current.show("Has cerrado la seleccion de imagenes")
            } else {
                uploadImage(result.uri).then(()=>{
                    toastRef.current.show("Imagen subida");
                    updatePhotoUrl();
                }).catch(()=>{
                    toastRef.current.show("Error al actualizar el avatar")
                })
            }
        }
    
    };

    const uploadImage = async (uri)=>{
        setloadingText("Actualizando avatar");
        setloading(true)
        const response = await fetch(uri);
        const blob = await response.blob();
        const ref = firebase.storage().ref().child(`avatar/${uid}`);
        return ref.put(blob);
    };

    const updatePhotoUrl = ()=>{
        firebase.storage().ref(`avatar/${uid}`)
        .getDownloadURL()
        .then( async (response) =>{
            const update = {
                photoURL: response
            };
            await firebase.auth().currentUser.updateProfile(update);
            setloading(false);
        })
        .catch(()=>{
            toastRef.current.show("Error al actualizar el avatar")
        })
    }
    return (
        <View style = {styles.viewUserInfo}>
           <Avatar
                size="large"
                source={photoURL ? { uri: photoURL} : require('../../../assets/avatar-default.jpg') }
                onPress={changeAvatar}
                rounded
                containerStyle={styles.userInfoAvatar}
                >
                
                <Accessory/>
            </Avatar>  
            
            <View>
                <Text style={styles.displayName}>
                    {displayName ? displayName : "Anonimo"} 
                </Text>
                <Text>
                    {email}
                </Text>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({  
    viewUserInfo: {
        alignItems: "center",
        justifyContent: "center",
        flexDirection: "row",
        backgroundColor: "#f2f2f2",
        paddingTop: 20,
        paddingBottom: 20
    },
    userInfoAvatar: {
        marginRight: 20,
    },
    displayName: {
        fontWeight: "bold",
        paddingBottom: 5,
    }
})