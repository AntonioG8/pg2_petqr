import React, {useState} from 'react'
import { View, Text,  StyleSheet } from 'react-native'
import { Input, Icon, Button } from 'react-native-elements'
import { validateEmail } from '../../utils/validations'
import { size, isEmpty } from 'lodash'
import * as firebase from 'firebase';
import { useNavigation } from '@react-navigation/native'
import Loading  from '../Loading'

export default function RegisterForm(props) {
    
    const {toastRef} = props;
    const [loading, setLoading] = useState(false);
    const [showPassword, setShowPassword] = useState(false);
    const [showRepeatPassword, setShowRepeatPassword] = useState(false);
    const [formData, setFormData]=useState(defaultFormValue());
    const navigation = useNavigation();

    const onSubmit = () =>{
        //console.log(formData);
        //console.log(validateEmail(formData.email))
        if(isEmpty(formData.email) || isEmpty(formData.password) || isEmpty(formData.repeatPassword)){
            //alert("Debe llenar todos los campos");
            toastRef.current.show("Todos los campos son obligatorios");
        }else if(!validateEmail(formData.email)){         
            toastRef.current.show("Email no es correcto");
        }else if(formData.password !== formData.repeatPassword){
            toastRef.current.show("Las contraseñas deben ser iguales");
        }else if(size(formData.password) < 6){
            toastRef.current.show("La contraseña debe tener al menos 6 caracteres");
        }else{
            //toastRef.current.show("Registrando");
            setLoading(true);
            firebase.auth().createUserWithEmailAndPassword(formData.email, formData.password)
            .then(() => {
                setLoading(false);
                navigation.navigate("Cuenta");
            })
            .catch(() => {
                setLoading(false);
                toastRef.current.show("Error en el registro");
            })
        }
    };
    //execute method for changes in the inputs of Register form
    const onChange = (e, type) =>{
        /*console.log(type);
        //console.log(e.nativeEvent.text);
        setFormData({[type]: e.nativeEvent.text}) methods for learning, with implements inputs in object data of the register form*/
        setFormData({...formData, [type]: e.nativeEvent.text })
    }

    return (
        <View>
            <Input
                textContentType="emailAddress"
                placeholder="Correo electronico"
                onChange = {(e)=> onChange(e, "email")}
                containerStyle={styles.inputForm}
                leftIcon={{type:'material-community', name:'email-outline'}}
            />
            <Input
                password={true}
                secureTextEntry={showPassword ? false : true}
                placeholder="Ingrese su contraseña"
                onChange = {(e)=> onChange(e, "password")}
                containerStyle={styles.inputForm}
                leftIcon={
                    <Icon
                    type='material-community'
                    name={showPassword ? "eye-off-outline" : "eye-outline"}
                    onPress={()=> setShowPassword(!showPassword)}
                    />
                }
            />
            <Input
                password={true}
                secureTextEntry={showRepeatPassword ? false : true}
                placeholder="Confirmar su contraseña"
                onChange = {(e)=> onChange(e, "repeatPassword")}
                containerStyle={styles.inputForm}
                leftIcon={
                    <Icon
                    type='material-community'
                    name={showRepeatPassword ? "eye-off-outline" : "eye-outline"}
                    onPress={()=>setShowRepeatPassword(!showRepeatPassword)}
                    />
                }
            />
            <Button
                title="Unirse"
                containerStyle={styles.btnContainerRegister}
                buttonStyle={styles.btnRegister}
                onPress= {onSubmit}
            />
            <Loading isVisible={loading} text="Registrando.." />
        </View>
    )
}

function defaultFormValue(){
    return {
        email: "",
        password: "",
        repeatPassword: ""
    }
}

const styles = StyleSheet.create({
    formContainer: {
        flex: 1,
        alignItems: "center",
        justifyContent: "center",
        marginTop: 30
    },
    inputForm: {
        width: "100%",
        marginTop: 20
    },
    btnContainerRegister: {
        marginTop: 20,
        width: "95%"
    },
    btnRegister: {
        backgroundColor:"#40B02E"
    }
})