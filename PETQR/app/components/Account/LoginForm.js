import React, {useState} from 'react'
import { View, Cam,StyleSheet } from 'react-native'
import {Input, Icon, Button} from 'react-native-elements'
import { isEmpty } from 'lodash'
import { validateEmail } from '../../utils/validations'
import * as firebase  from 'firebase'
import { useNavigation } from '@react-navigation/native'
import Loading from '../Loading' 
export default function LoginForm(props) {
    const {toastRef} = props;
    const [showPassword, setShowPassword] = useState(false);
    const [formData, setFormData]=useState(defaultFormValue());
    const [loading, setloading] = useState(false);
    const navigation = useNavigation();

    const onChange = (e, type) =>{
        /*console.log(type);
        //console.log(e.nativeEvent.text);
        setFormData({[type]: e.nativeEvent.text}) methods for learning, with implements inputs in object data of the LOGIN form*/
        setFormData({...formData, [type]: e.nativeEvent.text })
    }

    const onSubmit = () =>{
        //console.log(formData)
        
        if(isEmpty(formData.email) || isEmpty(formData.password)){
            toastRef.current.show("Todos los campos son obligatorios..")
        }else if(!validateEmail(formData.email)){
            toastRef.current.show("Email no valido")
        }else{
            setloading(true);
            firebase
                .auth()
                .signInWithEmailAndPassword(formData.email, formData.password)
                .then(()=>{
                    setloading(false);
                    navigation.navigate("Cuenta")
                })
                .catch(()=>{
                    setloading(false);
                    toastRef.current.show("Email o contraseña incorrecta")
                })
        }
    }
    return (
        <View style={styles.formContainer}>
            <Input 
                placeholder="Correo Electronico"
                onChange = {(e)=> onChange(e, "email")}
                containerStyle={styles.inputForm}
                leftIcon={{type:'material-community', name:'email-outline'}}
            />
            <Input
                placeholder="Contraseña"
                onChange = {(e)=> onChange(e, "password")}
                password ={true}
                secureTextEntry={showPassword ? false : true}
                containerStyle={styles.inputForm}
                leftIcon={
                    <Icon 
                    type="material-community" 
                    name={showPassword ? "eye-off-outline" : "eye-outline"}
                    onPress={()=> setShowPassword(!showPassword)}
                    />
                } 
            />
            <Button
                title="Iniciar sesion"
                containerStyle = {styles.btnContainerLogin}
                buttonStyle = {styles.btnLogin}
                onPress = {onSubmit}
            />
            <Loading isVisible={loading} text="Iniciando sesion.." />
        </View>
    )
}
function defaultFormValue(){
    return{
        email: "",
        password: ""
    }
}
const styles = StyleSheet.create({
    formContainer:{
        flex: 1,
        alignItems: "center",
        justifyContent: "center",
        marginTop: 30
    },
    inputForm: {
        width: "100%",
        marginTop: 20
    },
    btnContainerLogin: {
        marginTop: 20,
        width: "95%"
    },
    btnLogin: {
        backgroundColor: "#ff5050"
    }
})